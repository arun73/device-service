package com.khoubyari.example.dao.jpa;

import com.khoubyari.example.domain.Application;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Arun Kumar G (16535)
 */
public interface ApplicationRepository extends CrudRepository<Application, Long> {
}
