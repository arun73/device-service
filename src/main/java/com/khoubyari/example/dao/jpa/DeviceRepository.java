package com.khoubyari.example.dao.jpa;

import com.khoubyari.example.domain.Device;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Arun Kumar G (16535)
 */
public interface DeviceRepository extends CrudRepository<Device, Long> {
}
