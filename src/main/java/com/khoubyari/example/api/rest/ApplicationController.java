package com.khoubyari.example.api.rest;

import com.khoubyari.example.domain.Application;
import com.khoubyari.example.service.ApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * @author Arun Kumar G (16535)
 */
@RestController
@RequestMapping(value = "/applications")
public class ApplicationController {

    @Autowired
    private ApplicationService applicationService;

    @RequestMapping(value = "",
        method = RequestMethod.POST,
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"})
    @ResponseStatus(HttpStatus.CREATED)
    public Application create(@RequestBody Application application) {
        return applicationService.create(application);
    }
}
