package com.khoubyari.example.api.rest;

import com.khoubyari.example.domain.Device;
import com.khoubyari.example.domain.DeviceApplication;
import com.khoubyari.example.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author Arun Kumar G (16535)
 */
@RestController
@RequestMapping(value = "/devices")
public class DeviceController extends AbstractRestHandler {

    @Autowired
    private DeviceService deviceService;

    @RequestMapping(value = "",
        method = RequestMethod.POST,
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"})
    public Device create(@RequestBody Device device) {
        return deviceService.create(device);
    }

    @RequestMapping(value = "/{id}",
        method = RequestMethod.GET,
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"})
    public Device findById(@PathVariable("id") Long id) {
        return deviceService.findById(id);
    }

    @RequestMapping(value = "/{id}",
        method = RequestMethod.PUT,
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"})
    public Device update(@PathVariable("id") Long id, @RequestBody Device device) {
        return deviceService.update(id, device);
    }


    @RequestMapping(value = "/{deviceId}/application",
        method = RequestMethod.POST,
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"})
    public Device addApplication(@PathVariable("deviceId") String deviceId, DeviceApplication deviceApplication) {
        return deviceService.addApplication(deviceId, deviceApplication);
    }

    @RequestMapping(value = "/{deviceId}/application",
        method = RequestMethod.PUT,
        consumes = {"application/json", "application/xml"},
        produces = {"application/json", "application/xml"})
    public Device updateApplication(@PathVariable("deviceId") String deviceId, DeviceApplication deviceApplication) {
        return deviceService.updateApplication(deviceId, deviceApplication);
    }
}
