package com.khoubyari.example.service;

import com.khoubyari.example.dao.jpa.ApplicationRepository;
import com.khoubyari.example.domain.Application;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Arun Kumar G (16535)
 */
@Service
public class ApplicationService {
    @Autowired
    private ApplicationRepository applicationRepository;

    public Application create(Application application) {
        return applicationRepository.save(application);
    }

    public Application findById(Long id) {
        return applicationRepository.findOne(id);
    }
}
