package com.khoubyari.example.service;

import com.khoubyari.example.dao.jpa.ApplicationRepository;
import com.khoubyari.example.dao.jpa.DeviceRepository;
import com.khoubyari.example.domain.Device;
import com.khoubyari.example.domain.DeviceApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Arun Kumar G (16535)
 */
@Service
public class DeviceService {

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private ApplicationRepository applicationRepository;

    public Device create(Device device) {
        return deviceRepository.save(device);
    }

    public Device findById(Long id) {
        return deviceRepository.findOne(id);
    }

    public Device addApplication(String deviceId, DeviceApplication deviceApplication) {
        return null;
    }

    public Device updateApplication(String deviceId, DeviceApplication deviceApplication) {
        return null;
    }

    public Device update(Long id, Device device) {
        return null;
    }
}
