package com.khoubyari.example.domain;

import javax.persistence.*;

/**
 * @author Arun Kumar G (16535)
 */
@Entity
@Table(name = "application")
public class Application {

    @Id
    @GeneratedValue()
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "domain_name")
    private String domainName;

    @Column(name = "os_name")
    private OperatingSystem osName;

    @Column(name = "min_os_version")
    private String minOsVersion;

    @Column(name = "target_os_version")
    private String targetOsVersion;

    @Column(name = "latest_app_version")
    private String latestAppVersion;

    @Version
    @Column(name = "version")
    private Long version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    public OperatingSystem getOsName() {
        return osName;
    }

    public void setOsName(OperatingSystem osName) {
        this.osName = osName;
    }

    public String getMinOsVersion() {
        return minOsVersion;
    }

    public void setMinOsVersion(String minOsVersion) {
        this.minOsVersion = minOsVersion;
    }

    public String getTargetOsVersion() {
        return targetOsVersion;
    }

    public void setTargetOsVersion(String targetOsVersion) {
        this.targetOsVersion = targetOsVersion;
    }

    public String getLatestAppVersion() {
        return latestAppVersion;
    }

    public void setLatestAppVersion(String latestAppVersion) {
        this.latestAppVersion = latestAppVersion;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Application{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", domainName='" + domainName + '\'' +
            ", osName=" + osName +
            ", minOsVersion='" + minOsVersion + '\'' +
            ", targetOsVersion='" + targetOsVersion + '\'' +
            ", latestAppVersion='" + latestAppVersion + '\'' +
            ", version=" + version +
            '}';
    }
}
