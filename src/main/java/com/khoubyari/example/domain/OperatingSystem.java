package com.khoubyari.example.domain;

/**
 * @author Arun Kumar G (16535)
 */
public enum OperatingSystem {
    ANDROID,
    iOS
}
