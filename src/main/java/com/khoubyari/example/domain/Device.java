package com.khoubyari.example.domain;

import javax.persistence.*;
import java.util.List;

/**
 * @author Arun Kumar G (16535)
 */
@Entity
@Table(name = "device")
public class Device {

    @Id
    @GeneratedValue()
    private Long id;

    @Column(name = "imei")
    private Long imei;

    @Enumerated(EnumType.STRING)
    @Column(name = "os_name")
    private OperatingSystem osName;

    @Column(name = "os_version")
    private String osVersion;

    @Column(name = "manufacturer_name", updatable = false)
    private String manufacturerName;

    @Column(name = "model_name", updatable = false)
    private String modelName;

    @Column(name = "ram_size", updatable = false)
    private Integer ramSize;

    @Column(name = "rom_size", updatable = false)
    private Integer romSize;

    @OneToMany(mappedBy = "device")
    private List<DeviceApplication> deviceApplications;

    @Version
    @Column(name = "version")
    private Long version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getImei() {
        return imei;
    }

    public void setImei(Long imei) {
        this.imei = imei;
    }

    public OperatingSystem getOsName() {
        return osName;
    }

    public void setOsName(OperatingSystem osName) {
        this.osName = osName;
    }

    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    public String getManufacturerName() {
        return manufacturerName;
    }

    public void setManufacturerName(String manufacturerName) {
        this.manufacturerName = manufacturerName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Integer getRamSize() {
        return ramSize;
    }

    public void setRamSize(Integer ramSize) {
        this.ramSize = ramSize;
    }

    public Integer getRomSize() {
        return romSize;
    }

    public void setRomSize(Integer romSize) {
        this.romSize = romSize;
    }

    public List<DeviceApplication> getDeviceApplications() {
        return deviceApplications;
    }

    public void setDeviceApplications(List<DeviceApplication> deviceApplications) {
        this.deviceApplications = deviceApplications;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Device{" +
            "id=" + id +
            ", imei=" + imei +
            ", osName=" + osName +
            ", osVersion='" + osVersion + '\'' +
            ", manufacturerName='" + manufacturerName + '\'' +
            ", modelName='" + modelName + '\'' +
            ", ramSize=" + ramSize +
            ", romSize=" + romSize +
            ", deviceApplications=" + deviceApplications +
            ", version=" + version +
            '}';
    }
}
