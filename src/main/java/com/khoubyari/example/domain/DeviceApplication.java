package com.khoubyari.example.domain;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Arun Kumar G (16535)
 */
@Entity
@Table(name = "device_application")
public class DeviceApplication {
    @Id
    @GeneratedValue()
    private Long id;

    @ManyToOne
    @JoinColumn(name = "device_id")
    private Device device;

    @ManyToOne
    @JoinColumn(name = "application_id")
    private Application application;

    @Column(name = "app_version")
    private String appVersion;

    @Column(name = "first_install_date")
    private Date firstInstallDate;

    @Column(name = "last_updated_on")
    private Date lastUpdatedDate;

    @Column(name = "is_currently_installed")
    private Boolean isCurrentlyInstalled;

    @Version
    @Column(name = "version")
    private Long version;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public Date getFirstInstallDate() {
        return firstInstallDate;
    }

    public void setFirstInstallDate(Date firstInstallDate) {
        this.firstInstallDate = firstInstallDate;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public Boolean getCurrentlyInstalled() {
        return isCurrentlyInstalled;
    }

    public void setCurrentlyInstalled(Boolean currentlyInstalled) {
        isCurrentlyInstalled = currentlyInstalled;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "DeviceApplication{" +
            "id=" + id +
            ", device=" + device +
            ", application=" + application +
            ", appVersion='" + appVersion + '\'' +
            ", firstInstallDate=" + firstInstallDate +
            ", lastUpdatedDate=" + lastUpdatedDate +
            ", isCurrentlyInstalled=" + isCurrentlyInstalled +
            ", version=" + version +
            '}';
    }
}
